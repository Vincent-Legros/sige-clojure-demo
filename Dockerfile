FROM openjdk:8-alpine

COPY target/uberjar/sige-banque-db.jar /sige-banque-db/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/sige-banque-db/app.jar"]
