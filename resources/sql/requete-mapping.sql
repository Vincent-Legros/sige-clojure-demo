
-- :name select-mapping-id :? :1 
SELECT 
--~ (if (seq (:cols params)) ":i":cols" "*") 
FROM unite.ump_unite_mapping as ump 
where ump.ump_id = :id 


-- :name select-mapping-where :? :*  
SELECT 
--~ (if (seq (:cols params)) ":i*:cols" "*") 
FROM unite.ump_unite_mapping as ump   
INNER JOIN ref.tmg_type_mapping as tmg ON ump.tmg_code = tmg.tmg_code 
--~ (if  (:uac_id params) "where ump.uac_id = :uac_id" "where true")     

