-- :name select-unite :? :1  
SELECT  
--~ (if (seq (:cols params)) ":i*:cols" "*")
FROM unite.uac_unite_academique as uac 
WHERE uac.uac_id = :id    


-- :name select-unite-recherche :? :*  
-- :require [sige-banque-db.db.recherche-avancee :refer [recherche-date+]]  
SELECT  
--~ (if (seq (:cols params)) ":i*:cols" "*") 
FROM unite.uac_unite_academique as uac
where true
--~ (if (:id params) "AND uac.uac_id  = :id")
--~ (if (:code params) "AND uac.uac_code = :code")
--~ (if (:date_debut params) (recherche-date+ "uac.uac_date_debut" :date_debut params))
--~ (if (:date_fin  params) "AND uac.uac_date_fin  = :date_fin")
--~ (if (:date_creation params) "AND uac.uac_creation = :date_creation")
--~ (if (:titre_abrege params) "AND uac.uac_titre_abrege = :titre_abrege")
--~ (if (:titre params) "AND uac.uac_titre  = :titre")
--~ (if (:type params) "AND uac.tua_code = :type")
--~ (if (:etat params) "AND uac.eta_code = :etat") 

