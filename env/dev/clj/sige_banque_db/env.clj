(ns sige-banque-db.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [sige-banque-db.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[sige-banque-db started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[sige-banque-db has shut down successfully]=-"))
   :middleware wrap-dev})
