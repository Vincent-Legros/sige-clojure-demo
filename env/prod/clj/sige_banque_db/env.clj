(ns sige-banque-db.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[sige-banque-db started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[sige-banque-db has shut down successfully]=-"))
   :middleware identity})
