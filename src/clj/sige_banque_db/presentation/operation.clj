(ns sige-banque-db.presentation.operation
  (:require [clojure.string :as string]))

(defn string->keys [champs] 
  (vec(map keyword (string/split champs #","))))

(defn filtrer-reponse [champs reponse] 
  (if (not (nil? champs))
    (select-keys reponse (string->keys champs))
    reponse)) 

(defn filtrer-reponse-liste [champs liste-reponse]  
  (map (partial filtrer-reponse champs) liste-reponse))  

(defn trier-reponse [champs liste-reponse] 
  (if (not (nil? champs)) 
    (sort-by (apply juxt (string->keys champs) liste-reponse) 
   liste-reponse)))

(defn pagination [nbParPage numeroPage reponse] 
    (vec (nth (partition-all nbParPage reponse) (dec numeroPage) {}))) 
