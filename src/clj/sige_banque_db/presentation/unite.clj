(ns sige-banque-db.presentation.unite) 


(defn get-by-id [{uac :unite justs :just mappings :mappings } resultat ]   
   (array-map  
     :id ( :uac_id uac) 
     :id_racine nil 
     :code  ( :uac_code uac) 
     :date_debut (:uac_date_debut uac) 
     :date_fin  (:uac_date_fin uac) 
     :titre_abrege (:uac_titre_abrege uac) 
     :titre (:uac_titre uac) 
     :titre_publication (:uac_titre_publication uac)
     :remarque (:uac_remarque uac) 
     :etag (:uac_version_modif uac)  
     :type {  
           :code (:tua_code uac) 
           :description nil }
     :etat (:eta_code uac)  
     :etat_affichage   nil 
     :trimestre_debut { 
        :code (:trm_code_debut uac)
        :description nil 
      } 
      :trimestre_fin {
        :code (:trm_code_fin uac) 
        :description nil 
      } 
      :sequence  (:uac_sequence uac)))


; array-map keep the order of the key word 
(defn format-unite [uac]   
  ( array-map 
    :id (:uac_id uac)
    ;:id_racine ( )
    :code (:uac_code uac) 
    :date_debut (:uac_date_debut uac) 
    :date_fin  (:uac_date_fin uac) 
    :date_creation (:uac_date_creation uac)
    :titre_abrege (:uac_titre_abrege uac) 
    :titre (:uac_titre_officiel uac)
    :etag  (:uac_version_modif uac) 
    :type {
        :code (:tua_code uac)
        :description nil 
    }  
    :trimestre_debut { 
        :code (:trm_code_debut uac)
        :description nil 
    } 
    :trimestre_fin {
        :code (:trm_code_fin uac) 
        :description nil 
    } 
    :sequence  (:uac_sequence uac)) 
)  

(defn format-reponse-liste [uac-liste]  
  (map format-unite uac-liste))


