(ns sige-banque-db.db.recherche-avancee
  (:require [clojure.string :as s]))   

(defn- operateur-date [operateur-valeur]  
  (let [operator  { :gt ">"  :lt "<"  :eq  "=" :lte "<=" :gte ">=" :neq "<>" }]
    (->> operateur-valeur keys first (get operator)))) 

(defn- format-get [op-val]
 (->> op-val keys first name)) 

(defn  recherche-date+ [champsBD k params]  
  (let [ op-val (k params)]  
    (if (map? op-val) 
      (str "And" " " champsBD "::text "  (operateur-date op-val) " " k "." (format-get op-val)) 
      (str "And" " " champsBD "::text" " = " k  ))))  
