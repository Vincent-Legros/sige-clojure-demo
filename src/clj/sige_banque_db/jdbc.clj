(ns sige-banque-db.jdbc 
  (:require [clojure.java.jdbc :as sql]))


(def db {:subprotocol "postgresql"
         :subname "//localhost/banque"
         :user "sigeadmin"
         :password "sigepassword"}) 


(defn get-unite [id] 
  (first (sql/query db ["select * from unite.uac_unite_academique where uac_id = ?" id])))

(defn add-unite! [unite] 
  (sql/insert! db :unite unite)) 
