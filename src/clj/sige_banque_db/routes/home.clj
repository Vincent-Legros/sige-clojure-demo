(ns sige-banque-db.routes.home
  (:require
   [sige-banque-db.layout :as layout]
   [sige-banque-db.db.core :as db]
   [clojure.java.io :as io]
   [sige-banque-db.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]))

(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page [request]
  (layout/render request "about.html")) 

(defn unite [request] 
  ) 

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/about" {:get about-page}]])

