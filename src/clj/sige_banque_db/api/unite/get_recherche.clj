(ns sige-banque-db.api.unite.get-recherche
  (:require [sige-banque-db.db.core :as db :refer [dbspec *db*]]
            [clojure.java.jdbc :as jdbc]))    


(defn get-unite-recherche  [params] 
  (db/select-unite-recherche  params))
