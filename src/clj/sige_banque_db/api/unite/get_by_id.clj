(ns sige-banque-db.api.unite.get-by-id
 (:require [sige-banque-db.db.core :as db]
           [sige-banque-db.db.core :refer [dbspec *db*]]
           [clojure.java.jdbc :as jdbc]))   

(def champs-unite [["uac.uac_id" "id"] 
             ["uac.uac_code" "code"] 
             ["uac.uac_date_debut" "date_debut"]  
             ["uac.uac_date_fin" "date_fin"]
             ["uac.uac_date_creation" "date_creation"] 
             ["uac.uac_titre_abrege" "titre_abrege"] 
             ["uac.uac_titre_officiel" "titre"] 
             ["uac.uac_titre_publication" "titre_publication"] 
             ["uac.uac_remarque" "remarque"] 
             ["uac.uac_version_modif" "version"] 
             ["uac.tua_code" "code_type"] 
             ["uac.eta_code" "code_etat"] 
             ["uac.trm_code_debut" "code_trimestre_debut"]
             ["uac.trm_code_fin" "code_trimestre_fin"]
             ["uac.uac_sequence" "sequence"]
             ;["uac.uac_id_racine" "id_racine"]
             ])  

(def champs-jus [ 
    "jua.jus_id" 



]) 

(def champs-mapping [
    ["ump.ump_id" "id"]
    ["ump.ump_valeur" "valeur"]]
)

(defn get-unite [id]
  (db/select-unite {:id id }))  

(defn get-justification-liste [id] 
  (db/select-justification-where {:uac_id id })) 

(defn get-mapping-liste [id] 
  (db/select-mapping-where {:uac_id id }))  

(defn get-reponse-unite [id]   
  (jdbc/with-db-transaction  [ *db* dbspec ]
    {:unite (get-unite id) 
     :just (get-justification-liste id) 
     :mapping (get-mapping-liste id) }))
